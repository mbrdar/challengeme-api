CREATE TABLE challenges
(
    id         UUID NOT NULL,
    title       VARCHAR(255),
    PRIMARY KEY (id)
);
