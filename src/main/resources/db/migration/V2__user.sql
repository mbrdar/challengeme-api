CREATE TABLE users
(
    id                               UUID         NOT NULL,
    username                         VARCHAR(255) NOT NULL,
    password                         VARCHAR(255) NOT NULL,
    email                            VARCHAR(255) NOT NULL,
    active                           BOOLEAN      NOT NULL DEFAULT true,
    activated                        BOOLEAN      NOT NULL DEFAULT false,
    email_confirmation_token         UUID,
    reset_password_token             UUID,
    registration_date                TIMESTAMP    NOT NULL,
    reset_password_token_valid_until TIMESTAMP,
    PRIMARY KEY (id)
);
