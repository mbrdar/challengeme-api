package apa.rale.challengeme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class ChallengemeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengemeApplication.class, args);
	}

}
