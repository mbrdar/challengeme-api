package apa.rale.challengeme.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig implements WebMvcConfigurer {
  @Value("${cors.url}")
  private String corsUrl;

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**")
      .allowedOrigins(corsUrl)
      .exposedHeaders("Content-Disposition")
      .allowedMethods("POST", "PUT", "GET", "DELETE", "OPTIONS");
  }

}
