package apa.rale.challengeme.config.auth;

import apa.rale.challengeme.model.User;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@Component
public class JWTProvider {

  @Value("${security.secret}")
  private String securitySecret;
  public static final long EXPIRATION_TIME = 864_000_000; // 10 days
  public static final String TOKEN_PREFIX = "Bearer ";
  public static final String AUTHORIZATION_HEADER = "Authorization";
  public static final String SIGN_UP_URL = "/api/v1/users/sign-up";
  public static final String ROLE_USER = "ROLE_USER";

  public DecodedJWT verify(String token) {
    return JWT.require(Algorithm.HMAC512(securitySecret.getBytes()))
      .build()
      .verify(token.replace(JWTProvider.TOKEN_PREFIX, ""));
  }

  public String issueToken(User user) {
    return JWT.create()
      .withSubject(user.getUsername())
      .withClaim("role", ROLE_USER)
      .withClaim("userId", user.getId().toString())
      .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
      .sign(Algorithm.HMAC512(securitySecret.getBytes()));
  }
}
