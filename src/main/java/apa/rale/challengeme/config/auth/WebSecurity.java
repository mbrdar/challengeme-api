package apa.rale.challengeme.config.auth;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurity extends WebSecurityConfigurerAdapter {
  private final JWTProvider jwtProvider;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().authorizeRequests()
      .antMatchers("/api/**/**/*").permitAll()
      .and()
      .addFilterAfter(new JWTAuthenticationFilter(jwtProvider), UsernamePasswordAuthenticationFilter.class)
      // this disables session creation on Spring Security
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }

  @Override
  public void configure(org.springframework.security.config.annotation.web.builders.WebSecurity web) {
    web.ignoring().antMatchers(
      "/v2/api-docs",
      "/swagger-resources/**",
      "/configuration/security",
      "/swagger-ui.html",
      "/swagger",
      "/webjars/**",
      "/actuator/health"
    );

    web.ignoring()
      .regexMatchers(HttpMethod.GET,
        "/api/.+/auth/email-confirm/.*/.*"
      )
      .regexMatchers(HttpMethod.POST,
        "/api/.+/users/sign-up",
        "/api/.+/auth/login",
        "/api/.+/auth/forgot-password",
        "/api/.+/auth/reset-password"

      );
  }
}
