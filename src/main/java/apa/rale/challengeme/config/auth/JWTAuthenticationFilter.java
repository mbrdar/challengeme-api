package apa.rale.challengeme.config.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@RequiredArgsConstructor
public class JWTAuthenticationFilter extends GenericFilterBean {
  private static final String OPTIONS_HEADER = "OPTIONS";
  private final JWTProvider jwtProvider;

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest httpServletRequest = (HttpServletRequest) request;
    HttpServletResponse httpServletResponse = (HttpServletResponse) response;
    String jwtToken = httpServletRequest.getHeader(JWTProvider.AUTHORIZATION_HEADER);

    // Ignore OPTIONS header
    if (OPTIONS_HEADER.equals(httpServletRequest.getMethod())) {
      httpServletResponse.setStatus(HttpServletResponse.SC_OK);
    } else if (jwtToken == null) {
      httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    } else {
      var user = jwtProvider.verify(jwtToken).getSubject();

      if (user != null) {
        var authentication = new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
      }
      httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
  }
}
