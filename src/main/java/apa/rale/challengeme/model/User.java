package apa.rale.challengeme.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Table(name = "users")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;
  @NotBlank
  private String username;
  @NotBlank
  private String password;
  @NotBlank
  private String email;
  @NotNull
  private boolean active;
  @NotNull
  private boolean activated;

  @NotNull
  private LocalDateTime registrationDate;

  private UUID emailConfirmationToken;
  private UUID resetPasswordToken;

  private LocalDateTime resetPasswordTokenValidUntil;
}
