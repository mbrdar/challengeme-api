package apa.rale.challengeme.model;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "challenges")
public class Challenge {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;
  private String title;

}
