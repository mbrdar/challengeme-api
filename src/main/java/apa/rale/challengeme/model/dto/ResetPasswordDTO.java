package apa.rale.challengeme.model.dto;

import apa.rale.challengeme.validation.FieldMatch;
import apa.rale.challengeme.validation.ValidPassword;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match")
public class ResetPasswordDTO {
  @NotNull(message = "Token can't be empty!")
  private UUID token;
  @ValidPassword
  private String password;
  @ValidPassword
  private String confirmPassword;
  @Email(message = "Email invalid format!")
  @NotBlank(message = "Email can't be empty!")
  private String email;
}
