package apa.rale.challengeme.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
public class LoginDataDTO {
  @NotBlank(message = "Username can't be empty")
  private String username;
  @NotEmpty(message = "Password can't be empty!")
  private String password;
}
