package apa.rale.challengeme.model.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
public class ForgotPasswordDTO {
  @Email(message = "Email invalid format!")
  @NotBlank(message = "Email can't be empty.")
  private String email;
}
