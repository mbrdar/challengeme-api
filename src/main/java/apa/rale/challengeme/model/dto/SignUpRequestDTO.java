package apa.rale.challengeme.model.dto;

import apa.rale.challengeme.validation.FieldMatch;
import apa.rale.challengeme.validation.ValidPassword;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match")
public class SignUpRequestDTO {
  @NotBlank(message = "Username can't be empty!")
  private String username;
  @ValidPassword
  private String password;
  @ValidPassword
  private String confirmPassword;
  @Email(message = "Email invalid format!")
  @NotBlank(message = "Email can't be empty!")
  private String email;
}
