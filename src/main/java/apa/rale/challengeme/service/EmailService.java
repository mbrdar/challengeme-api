package apa.rale.challengeme.service;

import java.util.UUID;

public interface EmailService {
  void sendEmailConfirmationLink(String hostname, String email, String username, UUID emailConfirmationId);
  void forgotPassword(String email, String username, UUID resetPasswordId);
}
