package apa.rale.challengeme.service;

import apa.rale.challengeme.controller.error.BadCredentialsException;
import apa.rale.challengeme.model.dto.LoginDataDTO;

import java.util.UUID;

public interface AuthService {
  String authenticate(LoginDataDTO loginData) throws BadCredentialsException;
  void confirmEmail(String username, UUID emailConfirmationId);
  void forgotPassword(String email);
  void resetPassword(String email, UUID token, String newPassword);
}
