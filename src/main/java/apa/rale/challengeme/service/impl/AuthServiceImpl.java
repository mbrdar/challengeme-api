package apa.rale.challengeme.service.impl;

import apa.rale.challengeme.config.auth.JWTProvider;
import apa.rale.challengeme.controller.error.BadCredentialsException;
import apa.rale.challengeme.controller.error.ForgotPasswordException;
import apa.rale.challengeme.controller.error.UserNotReadyOrDeactivatedException;
import apa.rale.challengeme.model.dto.LoginDataDTO;
import apa.rale.challengeme.service.AuthService;
import apa.rale.challengeme.service.EmailService;
import apa.rale.challengeme.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {

  private final JWTProvider jwtProvider;
  private final UserService userService;
  private final BCryptPasswordEncoder bCryptPasswordEncoder;
  private final EmailService emailService;

  @Override
  public String authenticate(LoginDataDTO loginData) {
    var user = userService.loadUserByUsername(loginData.getUsername());

    if (user.isPresent() && !user.get().isActive() && !user.get().isActivated()) {
      throw new UserNotReadyOrDeactivatedException("User not activated yet or deactivated.");
    }

    return user.filter(u -> bCryptPasswordEncoder.matches(loginData.getPassword(), u.getPassword()))
      .map(jwtProvider::issueToken)
      .orElseThrow(BadCredentialsException::new);
  }

  @Override
  public void confirmEmail(String username, UUID emailConfirmationId) {
    log.info("Confirming email for user: {}", username);
    var user = userService.loadUserByUsernameAndEmailConfirmationId(username, emailConfirmationId);

    if (user.isPresent() && user.get().isActive()) {
      user.get().setActivated(true);
      user.get().setEmailConfirmationToken(null);
      log.info("Successfully confirmed user {} email", username);
      userService.update(user.get());
    } else {
      throw new UserNotReadyOrDeactivatedException("User doesn't exist or deactivated.");
    }
  }

  @Override
  public void forgotPassword(String email) {
    log.info("Setting reset password code for user: {}", email);
    var user = userService.loadUserByEmail(email);

    if (user.isPresent() && user.get().isActive() && user.get().isActivated()) {
      user.get().setResetPasswordToken(UUID.randomUUID());
      var validUntil = LocalDateTime.now().plusDays(1);
      user.get().setResetPasswordTokenValidUntil(validUntil);
      userService.update(user.get());
      emailService.forgotPassword(email, user.get().getUsername(), user.get().getResetPasswordToken());
    } else {
      throw new UserNotReadyOrDeactivatedException("User doesn't exist or deactivated.");
    }
  }

  @Override
  public void resetPassword(String email, UUID token, String newPassword) {
    log.info("Resetting password for {}", email);
    var user = userService.loadUserByEmail(email);

    if (user.isPresent() && user.get().isActive() && user.get().isActivated()) {
      if (user.get().getResetPasswordToken().equals(token) && LocalDateTime.now().isBefore(user.get().getResetPasswordTokenValidUntil())) {
        user.get().setPassword(bCryptPasswordEncoder.encode(newPassword));
        user.get().setResetPasswordToken(null);
        user.get().setResetPasswordTokenValidUntil(null);
        userService.update(user.get());
        log.info("Password changed for {}", email);
      } else {
        throw new ForgotPasswordException("Forgot password token wrong or expired.");
      }
    } else {
      throw new UserNotReadyOrDeactivatedException("User doesn't exist or deactivated.");
    }
  }
}
