package apa.rale.challengeme.service.impl;

import apa.rale.challengeme.service.EmailService;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailServiceImpl implements EmailService {

  private final JavaMailSender emailSender;
  private final Configuration freemarkerConfig;

  @Async
  @Override
  public void sendEmailConfirmationLink(String hostname, String email, String username, UUID emailConfirmationId) {
    log.info("Sending email confirmation link");

    var model = new HashMap<String, Object>();
    model.put("username", username);
    model.put("confirmLink", hostname + "/api/v1/auth/email-confirm/" + username + "/" + emailConfirmationId.toString());

    sendEmail(model, "email-confirm-template.ftl", email,"ChallengeME - Confirm email");
  }

  @Async
  @Override
  public void forgotPassword(String email, String username, UUID resetPasswordId) {
    log.info("Sending reset password id for user: {}", username);

    var model = new HashMap<String, Object>();
    model.put("username", username);
    model.put("resetPasswordId", resetPasswordId);
    sendEmail(model, "email-forgot-password-template.ftl", email, "ChallengeME - Reset password");

  }

  private void sendEmail(HashMap<String, Object> model, String emailTemplate, String email, String emailSubject) {
    var message = emailSender.createMimeMessage();
    var helper = new MimeMessageHelper(message);

    freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");

    try {
      var template = freemarkerConfig.getTemplate(emailTemplate);
      var emailContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
      helper.setTo(email);
      helper.setText(emailContent, true);
      helper.setSubject(emailSubject);
      emailSender.send(message);
    } catch (IOException | TemplateException | MessagingException e) {
      log.error("Error while processing email template!", e);
    }
  }
}
