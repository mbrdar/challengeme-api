package apa.rale.challengeme.service.impl;

import apa.rale.challengeme.controller.error.UsernameOrEmailUnavailableException;
import apa.rale.challengeme.model.User;
import apa.rale.challengeme.model.dto.SignUpRequestDTO;
import apa.rale.challengeme.model.dto.UserDTO;
import apa.rale.challengeme.repository.UserRepository;
import apa.rale.challengeme.service.EmailService;
import apa.rale.challengeme.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
  private final UserRepository userRepository;
  private final BCryptPasswordEncoder bCryptPasswordEncoder;
  private final EmailService emailService;

  public Optional<User> loadUserByUsername(String username) {
    return userRepository.findByUsername(username);
  }

  public Optional<User> loadUserByUsernameAndEmailConfirmationId(String username, UUID emailConfirmationId) {
    return userRepository.findByUsernameAndEmailConfirmationToken(username, emailConfirmationId);
  }

  public Optional<User> loadUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  @Override
  public Optional<User> update(User user) {
    return Optional.of(userRepository.save(user));
  }

  @Override
  public UserDTO create(SignUpRequestDTO newUser, String hostname) {
    newUser.setPassword(bCryptPasswordEncoder.encode(newUser.getPassword()));

    if (userRepository.findByEmail(newUser.getEmail()).isPresent()) {
      throw new UsernameOrEmailUnavailableException("Email is already taken.");
    }

    if (userRepository.findByUsername(newUser.getUsername()).isPresent()) {
      throw new UsernameOrEmailUnavailableException("Username is already taken.");
    }
    var savedUser = userRepository.save(signUpUserToUser(newUser));
    emailService.sendEmailConfirmationLink(hostname, savedUser.getEmail(), savedUser.getUsername(), savedUser.getEmailConfirmationToken());
    return toDTO(savedUser);
  }


  private UserDTO toDTO(User user) {
    var userDTO = new UserDTO();
    userDTO.setId(user.getId());
    userDTO.setUsername(user.getUsername());
    userDTO.setEmail(user.getEmail());
    return userDTO;
  }

  private User signUpUserToUser(SignUpRequestDTO newUser) {
    var user = new User();
    user.setActive(true);
    user.setActivated(false);
    user.setUsername(newUser.getUsername());
    user.setEmail(newUser.getEmail());
    user.setPassword(newUser.getPassword());
    user.setEmailConfirmationToken(UUID.randomUUID());
    user.setRegistrationDate(LocalDateTime.now());
    return user;
  }
}
