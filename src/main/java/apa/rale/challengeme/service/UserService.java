package apa.rale.challengeme.service;

import apa.rale.challengeme.model.User;
import apa.rale.challengeme.model.dto.SignUpRequestDTO;
import apa.rale.challengeme.model.dto.UserDTO;

import java.util.Optional;
import java.util.UUID;

public interface UserService {
  Optional<User> loadUserByUsername(String username);
  UserDTO create(SignUpRequestDTO newUser, String hostname);
  Optional<User> loadUserByUsernameAndEmailConfirmationId(String username, UUID emailConfirmationId);
  Optional<User> loadUserByEmail(String email);
  Optional<User> update(User user);
}
