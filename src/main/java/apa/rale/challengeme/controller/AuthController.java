package apa.rale.challengeme.controller;

import apa.rale.challengeme.controller.error.ErrorResponse;
import apa.rale.challengeme.controller.error.ForgotPasswordException;
import apa.rale.challengeme.controller.error.UserNotReadyOrDeactivatedException;
import apa.rale.challengeme.controller.error.enumeration.ClientErrorType;
import apa.rale.challengeme.model.dto.ForgotPasswordDTO;
import apa.rale.challengeme.model.dto.LoginDataDTO;
import apa.rale.challengeme.model.dto.ResetPasswordDTO;
import apa.rale.challengeme.service.AuthService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/auth")
@Validated
@Slf4j
@RequiredArgsConstructor
public class AuthController {

  private final AuthService authService;

  @ApiOperation(value = "Login with username and password")
  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Will return jwt token"),
    @ApiResponse(code = 400, message = "Unable to process request because of invalid data", response = ErrorResponse.class),
    @ApiResponse(code = 422, message = "Unable to process request because of validation error", response = ErrorResponse.class)
  })
  @PostMapping("/login")
  public String login(@RequestBody LoginDataDTO loginData) {
    log.info("Request to login user: {}", loginData.getUsername());
    return this.authService.authenticate(loginData);
  }

  @ApiOperation(value = "Confirm email")
  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Will confirm email successfully"),
    @ApiResponse(code = 400, message = "Unable to process request because of invalid data", response = ErrorResponse.class),
    @ApiResponse(code = 422, message = "Unable to process request because of validation error", response = ErrorResponse.class)
  })
  @GetMapping("/email-confirm/{username}/{confirmId}")
  public void confirmEmail(@PathVariable String username, @PathVariable UUID confirmId) {
    log.info("Request to confirm email form user: {}", username);
    this.authService.confirmEmail(username, confirmId);
  }

  @ApiOperation(value = "Forgot password")
  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Will set forgot password token and send it to email"),
    @ApiResponse(code = 400, message = "Unable to process request because of invalid data", response = ErrorResponse.class),
    @ApiResponse(code = 422, message = "Unable to process request because of validation error", response = ErrorResponse.class)
  })
  @PostMapping("/forgot-password")
  public void forgotPassword(@Valid @RequestBody ForgotPasswordDTO forgotPassword) {
    log.info("Request to set forgot password token for user: {}", forgotPassword.getEmail());
    this.authService.forgotPassword(forgotPassword.getEmail());
  }

  @ApiOperation(value = "Reset password")
  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Will reset password, token is needed"),
    @ApiResponse(code = 400, message = "Unable to process request because of invalid data", response = ErrorResponse.class),
    @ApiResponse(code = 422, message = "Unable to process request because of validation error", response = ErrorResponse.class)
  })
  @PostMapping("/reset-password")
  public void resetPassword(@Valid @RequestBody ResetPasswordDTO resetPassword) {
    log.info("Request to reset password for user: {}", resetPassword.getEmail());
    this.authService.resetPassword(resetPassword.getEmail(), resetPassword.getToken(), resetPassword.getPassword());
  }

  @ExceptionHandler(UserNotReadyOrDeactivatedException.class)
  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  public ResponseEntity<ErrorResponse> userNotActivatedError(UserNotReadyOrDeactivatedException exception) {
    log.error("User not activated or deactivated {}", exception);

    var errorType = ClientErrorType.FORBIDDEN;

    var errorResponse = new ErrorResponse(errorType, exception.getMessage());
    return new ResponseEntity<>(errorResponse, errorType.getHttpStatus());
  }

  @ExceptionHandler(ForgotPasswordException.class)
  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  public ResponseEntity<ErrorResponse> forgotPasswordError(ForgotPasswordException exception) {
    log.error("Forgot password error {}", exception);

    var errorType = ClientErrorType.INVALID_DATA;

    var errorResponse = new ErrorResponse(errorType, exception.getMessage());
    return new ResponseEntity<>(errorResponse, errorType.getHttpStatus());
  }
}
