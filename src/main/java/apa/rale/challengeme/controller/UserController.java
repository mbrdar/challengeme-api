package apa.rale.challengeme.controller;

import apa.rale.challengeme.controller.error.ErrorResponse;
import apa.rale.challengeme.controller.error.UsernameOrEmailUnavailableException;
import apa.rale.challengeme.controller.error.enumeration.ClientErrorType;
import apa.rale.challengeme.model.dto.SignUpRequestDTO;
import apa.rale.challengeme.model.dto.UserDTO;
import apa.rale.challengeme.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@Slf4j
@Validated
public class UserController {

  private final UserService userService;

  @ApiOperation(value = "Sign up new user")
  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Will return new user"),
    @ApiResponse(code = 400, message = "Unable to process request because of invalid data", response = ErrorResponse.class),
    @ApiResponse(code = 422, message = "Unable to process request because of validation error", response = ErrorResponse.class)
  })
  @PostMapping("/sign-up")
  public UserDTO signUp(@Valid @RequestBody SignUpRequestDTO newUser, HttpServletRequest request) {
    return userService.create(newUser, getHost(request.getRequestURL().toString()));
  }

  @ExceptionHandler(UsernameOrEmailUnavailableException.class)
  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  public ResponseEntity<ErrorResponse> usernameOrEmailUnavailable(UsernameOrEmailUnavailableException exception) {
    log.error("Username or email unavailable {}", exception);

    var errorType = ClientErrorType.VALIDATION_ERROR;

    var errorResponse = new ErrorResponse(errorType, exception.getMessage());
    return new ResponseEntity<>(errorResponse, errorType.getHttpStatus());
  }

  private String getHost(String serverName) {
    return serverName.split("/api")[0];
  }
}
