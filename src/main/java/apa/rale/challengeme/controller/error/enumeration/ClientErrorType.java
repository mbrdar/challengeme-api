package apa.rale.challengeme.controller.error.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * Values of this enum will be returned to the client when there is a client error.
 */
@Getter
@RequiredArgsConstructor
public enum ClientErrorType {
  VALIDATION_ERROR(HttpStatus.UNPROCESSABLE_ENTITY, "There are validation errors in the request"),
  BAD_CREDENTIALS(HttpStatus.BAD_REQUEST, "Username and password do not match any user"),
  INVALID_DATA(HttpStatus.BAD_REQUEST, "Invalid data"),
  FORBIDDEN(HttpStatus.FORBIDDEN, "Operation is forbidden");
    /**
     * The status code that will be returned to the client. Must be a valid 4xx HTTP status code.
     */
    private final HttpStatus httpStatus;

    private final String message;
}
