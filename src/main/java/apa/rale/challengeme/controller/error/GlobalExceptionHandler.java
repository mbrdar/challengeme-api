package apa.rale.challengeme.controller.error;

import apa.rale.challengeme.controller.error.enumeration.ClientErrorType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  public ResponseEntity<ErrorResponse> methodArgumentNotValidException(ConstraintViolationException constraintException) {
    log.error("Invalid parameters {}", constraintException);

    var constraintViolation = constraintException.getConstraintViolations().stream().findFirst();

    var errorType = ClientErrorType.VALIDATION_ERROR;

    var errorResponse = new ErrorResponse(errorType, constraintViolation.isPresent() ?
      constraintViolation.get().getMessage() : errorType.getMessage());
    return new ResponseEntity<>(errorResponse, errorType.getHttpStatus());
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  public ResponseEntity<ErrorResponse> methodArgumentNotValidException(MethodArgumentNotValidException methodArgumentNotValidException) {
    log.error("Invalid parameters {}", methodArgumentNotValidException);

    var error = methodArgumentNotValidException.getBindingResult().getAllErrors().stream().findFirst();

    var errorType = ClientErrorType.VALIDATION_ERROR;

    var errorResponse = new ErrorResponse(errorType, error.isPresent() ?
      error.get().getDefaultMessage() : errorType.getMessage());
    return new ResponseEntity<>(errorResponse, errorType.getHttpStatus());
  }

  @ExceptionHandler(BadCredentialsException.class)
  public ResponseEntity<ErrorResponse> barCredentialException(RuntimeException exception) {
    log.error("Bad credentials", exception);
    var errorType = ClientErrorType.BAD_CREDENTIALS;

    var message = exception.getMessage() != null ? exception.getMessage() : errorType.getMessage();

    var errorResponse = new ErrorResponse(errorType, message);
    return new ResponseEntity<>(errorResponse, errorType.getHttpStatus());
  }
}
