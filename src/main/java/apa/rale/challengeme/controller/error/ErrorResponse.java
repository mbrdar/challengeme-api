package apa.rale.challengeme.controller.error;

import apa.rale.challengeme.controller.error.enumeration.ClientErrorType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponse {
  private ClientErrorType type;
  private String message;
}
