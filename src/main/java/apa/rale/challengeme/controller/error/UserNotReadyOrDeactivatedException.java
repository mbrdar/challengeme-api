package apa.rale.challengeme.controller.error;

public class UserNotReadyOrDeactivatedException extends RuntimeException {
  public UserNotReadyOrDeactivatedException(String message) {
    super(message);
  }
}
