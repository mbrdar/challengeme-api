package apa.rale.challengeme.controller.error;

public class ForgotPasswordException extends RuntimeException {
  public ForgotPasswordException(String message) {
    super(message);
  }
}
