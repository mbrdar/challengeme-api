package apa.rale.challengeme.controller.error;

public class BadCredentialsException extends RuntimeException {
  public BadCredentialsException(String message) {
    super(message);
  }

  public BadCredentialsException() {
    super();
  }
}
