package apa.rale.challengeme.controller.error;

public class UsernameOrEmailUnavailableException extends RuntimeException {
  public UsernameOrEmailUnavailableException(String message) {
    super(message);
  }
}
